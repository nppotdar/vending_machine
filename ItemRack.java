/*
 * ItemRack defines the rack in a Vending Machine & its general attributes
 *
*/

class ItemRack{
    
    //Code restrictions
    //input string = "BK34"  i = 0, 1, 
    static char charSet[] = {'A', 'B'}; // j = 0, 1 
    static char digitSet[] = {'1', '2', '3'};
    static int codeLength = 4;

    private String code;
    String name;
    String category;
    double price;
    int count;
    
    ItemRack(String code, String name, String category, double price, int count){
	this.code = code;
	this.name = name;
	this.category = category;
	this.price = price;
	this.count = count;
    }

    //getters
    public String getCode(){
	return this.code;
    }

    public String getName(){
	return this.name;
    }

    public String getCategory(){
	return this.category;
    }

    public double getPrice(){
	return this.price;
    }

    public int getCount(){
	return this.count;
    }


    //setters
    public void setCode( String code ){
	//step 1: check the code length
	if( code.length() != codeLength ){
	    System.out.println(this.code + " failed change " + code);
	    return;
	}
	    
	
	//step 2: whether first two letters are valid letters
	//code = "BK34"
	//charSet = ['A', 'B']
	for( int i = 0; i < 2; i++ ){
	    //i: 0, character from code = 'B'
	    //i: 1
	    char charSelectedFromCode = code.charAt(i);

	    int j = 0;
	    while( j < charSet.length){
		//j: 0, character from charSet = 'A'
		//j: 1, character from charSet = 'B'
		char charInValidSet = charSet[j];
		if( charSelectedFromCode == charInValidSet )
		    break;
		j++;
	    }

	    if( j >= charSet.length ){
		System.out.println("Black Friday's over scrum: Invalid code entry " + code);
		return;
	    }

	}


	//step 3: Checking out the last two characters for digits
	//code = "BK34"
	//intSet = [ 1, 2, 3 ]
	for( int i = 2; i < 4; i++ ){
	    //i: 2, character from code = '3'
	    //i: 3, character from code = '4'
	    char charSelectedFromCode = code.charAt(i);

	    int j = 0;
	    while( j < digitSet.length){
		//j: 0, character from digitSet = 1
		//j: 1, character from digitSet = 2
		//j: 2, character from digitSet = 3
		char charInValidSet = digitSet[j];
		if( charSelectedFromCode == charInValidSet )
		    break;
		j++;
	    }

	    if( j >= digitSet.length ){
		System.out.println("Black Friday's over scrum: Invalid code entry " + code);
		return;
	    }

	}

	System.out.println("Code changed successfully from " + this.code + " to " + code);
	this.code = code;
    }

    public void setName( String name ){
	this.name = name;
    }

    public void setCategory( String category ){
	this.category = category;
    }

    public void setPrice( double price ){
	this.price = price;
    }

    public void setCount( int count ){
	this.count = count;
    }
    


    public void printObject(){
	System.out.println(this.name);
	System.out.println(this.category);
	System.out.println(this.code);
	System.out.println(this.price);
	System.out.println(this.count);
	System.out.println();
    }

}
